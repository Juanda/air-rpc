package cn.atcoder.air.exception;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 7:37 PM
 */
public class ConnectAuthException extends RuntimeException implements Serializable {

    private String auth;

    public ConnectAuthException(String auth) {
        super("auth code:[" + auth+"] authentication failed");
        this.auth = auth;
    }
}
