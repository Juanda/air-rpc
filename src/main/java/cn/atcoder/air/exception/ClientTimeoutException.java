package cn.atcoder.air.exception;

import cn.atcoder.air.msg.MessageHeader;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 6:10 PM
 */
public class ClientTimeoutException extends MessageException implements Serializable {

    public ClientTimeoutException(MessageHeader header, Throwable cause) {
        super(header, cause);
    }

    public ClientTimeoutException(MessageHeader header, String errorMsg) {
        super(header, errorMsg);
    }

    public ClientTimeoutException(String errorMsg) {
        super(errorMsg);
    }
}
