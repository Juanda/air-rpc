package cn.atcoder.air.exception;

import cn.atcoder.air.msg.MessageHeader;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 6:06 PM
 */
public class MessageException extends RuntimeException implements Serializable {

    private int errorCode = 2;

    protected String errorMsg;

    private transient MessageHeader header;

    public MessageException(MessageHeader header, Throwable e) {
        super(e);
        this.header = header;
    }

    public MessageException(MessageHeader header, String errorMsg) {
        super(errorMsg);
        this.header = header;
        this.errorMsg = errorMsg;
    }

    protected MessageException(Throwable e) {
        super(e);
    }

    public MessageException(String errorMsg) {
        super(errorMsg);
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }
}
