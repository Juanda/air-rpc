package cn.atcoder.air.exception;

import cn.atcoder.air.msg.Invocation;
import cn.atcoder.air.msg.MessageHeader;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 6:10 PM
 */
public class NoProviderException extends MessageException implements Serializable {

    private Invocation invocationBody;

    public NoProviderException(Invocation invocationBody, MessageHeader header, String errorMsg) {
        super(header, errorMsg);
        this.invocationBody = invocationBody;
    }

    public NoProviderException(Invocation invocationBody) {
        super("No provider");
        this.invocationBody = invocationBody;
    }

    public NoProviderException(String errorMsg) {
        super(errorMsg);
    }

    public String getClazzName() {
        return invocationBody.getClazzName();
    }

    public String getMethodName() {
        return invocationBody.getMethodName();
    }
}
