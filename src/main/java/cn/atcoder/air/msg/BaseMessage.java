package cn.atcoder.air.msg;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 4:56 PM
 */
public class BaseMessage implements Serializable {

    private MessageHeader header = new MessageHeader();

    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        MessageType msgType = getMessageType();
        this.header = header;
        setMessageType(msgType);
    }

    public boolean isHeartBeat() {
        MessageType msgType = header.getMessageType();
        return msgType == MessageType.HEARTBEAT_REQUEST_MSG
                || msgType == MessageType.HEARTBEAT_RESPONSE_MSG;
    }

    public MessageType getMessageType() {
        return header.getMessageType();
    }

    public void setMessageType(MessageType messageType) {
        header.setMessageType(messageType);
    }

    public String getClientId() {
        return header.getClientId();
    }

    public void setClientId(String clientId) {
        header.setClientId(clientId);
    }

    public long getMessageId() {
        return header.getMessageId();
    }

    public void setMessageId(long messageId) {
        header.setMessageId(messageId);
    }

    @Override
    public String toString() {
        return "BaseMessage{" +
                "header=" + header +
                '}';
    }
}
