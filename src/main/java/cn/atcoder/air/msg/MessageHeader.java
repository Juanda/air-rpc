package cn.atcoder.air.msg;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description 消息头
 * @date 5/9/19 4:48 PM
 */
public class MessageHeader implements Serializable, Cloneable {

    private MessageType messageType;

    private String clientId;

    private long messageId;

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public MessageHeader copyHeader(MessageHeader header){
        this.messageId = header.messageId;
        this.clientId = header.clientId;
        return this;
    }
    /**
     * 克隆后和整体原来不是一个对象，
     * 属性相同，修改当前属性不会改变原来的
     * map和原来是一个对象，修改当前map也会改原来的
     *
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    public MessageHeader clone() {
        MessageHeader header;
        try {
            header = (MessageHeader) super.clone();
        } catch (CloneNotSupportedException e) {
            header = new MessageHeader();
            header.copyHeader(this);
        }
        return header;
    }

    @Override
    public String toString() {
        return "{" +
                "messageType=" + messageType +
                ", clientId='" + clientId + '\'' +
                ", messageId=" + messageId +
                '}';
    }
}
