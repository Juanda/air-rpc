package cn.atcoder.air.msg;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 5:04 PM
 */
public class RegisterRequestMessage extends RequestMessage implements Serializable {

    public RegisterRequestMessage() {
        super();
        setMessageType(MessageType.REGISTER_REQUEST_MSG);
    }
}
