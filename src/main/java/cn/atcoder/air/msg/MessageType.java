package cn.atcoder.air.msg;

/**
 * @author yangjunda1
 * @description 消息类型
 * @date 5/9/19 4:49 PM
 */
public enum MessageType {

    /**
     * 消息类型
     */
    HEARTBEAT_REQUEST_MSG,
    HEARTBEAT_RESPONSE_MSG,
    REGISTER_REQUEST_MSG,
    REGISTER_RESPONSE_MSG,
    CALLBACK_REQUEST_MSG,
    CALLBACK_RESPONSE_MSG

}
