package cn.atcoder.air.msg;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 5:01 PM
 */
public class ResponseMessage extends BaseMessage implements Serializable {

    private Throwable exception;

    public Throwable getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }

    /**
     * @return the error
     */
    public boolean isError() {
        return exception != null;
    }

    @Override
    public String toString() {
        return "ResponseMessage{" +
                "exception=" + exception +
                '}';
    }
}
