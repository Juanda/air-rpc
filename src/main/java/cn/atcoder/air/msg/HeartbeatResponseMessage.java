package cn.atcoder.air.msg;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 5:02 PM
 */
public class HeartbeatResponseMessage extends ResponseMessage implements Serializable {

    public HeartbeatResponseMessage() {
        super();
        setMessageType(MessageType.HEARTBEAT_RESPONSE_MSG);
    }

}
