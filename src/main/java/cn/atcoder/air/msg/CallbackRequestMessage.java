package cn.atcoder.air.msg;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 5:09 PM
 */
public class CallbackRequestMessage extends RequestMessage implements Serializable {

    private Object body;

    private Invocation invocationBody = new Invocation();

    public CallbackRequestMessage() {
        super();
        setMessageType(MessageType.CALLBACK_REQUEST_MSG);
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public Invocation getInvocationBody() {
        return invocationBody;
    }

    public void setInvocationBody(Invocation invocationBody) {
        this.invocationBody = invocationBody;
    }

    public String getClazzName() {
        return invocationBody.getClazzName();
    }

    public void setClazzName(String clazzName) {
        invocationBody.setClazzName(clazzName);
    }

    public String getMethodName() {
        return invocationBody.getMethodName();
    }

    public void setMethodName(String methodName) {
        invocationBody.setMethodName(methodName);
    }

    public String getAlias() {
        return invocationBody.getAlias();
    }

    public void setAlias(String alias) {
        invocationBody.setAlias(alias);
    }

    public String[] getArgsType() {
        return invocationBody.getArgsType();
    }

    public void setArgsType(String[] argsType) {
        invocationBody.setArgsType(argsType);
    }

    public Class[] getArgClasses() {
        return invocationBody.getArgClasses();
    }

    public void setArgClasses(Class[] argClasses) {
        invocationBody.setArgClasses(argClasses);
    }

    public Object[] getArgs() {
        return invocationBody.getArgs();
    }

    public void setArgs(Object[] args) {
        invocationBody.setArgs(args);
    }
}
