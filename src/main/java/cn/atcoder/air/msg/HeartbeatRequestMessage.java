package cn.atcoder.air.msg;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 5:02 PM
 */
public class HeartbeatRequestMessage extends RequestMessage implements Serializable {

    public HeartbeatRequestMessage() {
        super();
        setMessageType(MessageType.HEARTBEAT_REQUEST_MSG);
    }

}
