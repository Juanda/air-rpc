package cn.atcoder.air.msg;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 5:06 PM
 */
public class RegisterResponseMessage extends ResponseMessage implements Serializable {

    private String loginToken;

    private boolean success = false;

    public RegisterResponseMessage() {
        super();
        setMessageType(MessageType.REGISTER_RESPONSE_MSG);
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
