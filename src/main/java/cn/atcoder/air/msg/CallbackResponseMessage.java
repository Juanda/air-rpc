package cn.atcoder.air.msg;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 5:12 PM
 */
public class CallbackResponseMessage extends ResponseMessage implements Serializable {

    private Object body;

    public CallbackResponseMessage() {
        super();
        setMessageType(MessageType.CALLBACK_RESPONSE_MSG);
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }


}
