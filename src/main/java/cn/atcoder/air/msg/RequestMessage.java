package cn.atcoder.air.msg;

import java.io.Serializable;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 4:58 PM
 */
public class RequestMessage extends BaseMessage implements Serializable {

    private String auth;

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }
}
