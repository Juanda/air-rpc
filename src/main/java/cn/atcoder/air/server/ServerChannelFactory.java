package cn.atcoder.air.server;


import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import io.netty.channel.socket.SocketChannel;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ServerChannelFactory {

    private static Map<String, SocketChannel> channelMap = new ConcurrentHashMap<>();

    public static String add(SocketChannel socketChannel) {
        ChannelId channelId = socketChannel.id();
        String idStr = channelId.asLongText();
        channelMap.put(idStr, socketChannel);
        return idStr;
    }

    public static Channel get(String clientId) {
        return channelMap.get(clientId);
    }

    public static void remove(SocketChannel socketChannel) {
        ChannelId channelId = socketChannel.id();
        String idStr = channelId.asLongText();
        channelMap.remove(idStr);
    }

    public static int size() {
        return channelMap.size();
    }

    public static Set<String> keys() {
        return channelMap.keySet();
    }

}
