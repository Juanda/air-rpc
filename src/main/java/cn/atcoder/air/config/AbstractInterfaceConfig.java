package cn.atcoder.air.config;

/**
 * @author yangjunda1
 * @description
 * @date 5/10/19 3:27 PM
 */
public abstract class AbstractInterfaceConfig<T> {

    protected String interfaceId;

    protected Class<T> clazz;

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId;
        try {
            this.clazz = (Class<T>) Class.forName(interfaceId);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
        this.interfaceId = clazz.getName();
    }
}
