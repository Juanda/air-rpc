package cn.atcoder.air.config;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yangjunda1
 * @description
 * @date 5/10/19 3:33 PM
 */
public class ProviderFactory {

    private static final ConcurrentHashMap<String, ProviderConfig> PROVIDER_CLAZZS = new ConcurrentHashMap<>();

    public static void cacheProviderConfig(ProviderConfig providerConfig) {
        PROVIDER_CLAZZS.put(providerConfig.getInterfaceId(), providerConfig);
    }

    public static ProviderConfig getProvider(String ifaceId) {
        final ProviderConfig providerConfig = PROVIDER_CLAZZS.get(ifaceId);
        if (providerConfig == null) {
            throw new NullPointerException();
        }
        return providerConfig;
    }

    public static boolean contains(String ifaceId) {
        return PROVIDER_CLAZZS.containsKey(ifaceId);
    }
}
