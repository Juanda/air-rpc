package cn.atcoder.air.transport;

import java.net.InetSocketAddress;

/**
 * @author yangjunda1
 * @description todo
 * @date 2019/1/25 10:16
 */
public interface ClientTransport extends BaseTransport{

    /**
     * 得到连接的远端地址
     *
     * @return the remote address
     */
    String getRemoteAddress();

    /**
     * 当前请求数
     *
     * @return 当前请求数
     */
    long currentRequests();

}
