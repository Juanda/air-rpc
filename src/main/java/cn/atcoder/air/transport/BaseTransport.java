package cn.atcoder.air.transport;

import cn.atcoder.air.msg.BaseMessage;
import cn.atcoder.air.msg.RequestMessage;
import cn.atcoder.air.msg.ResponseMessage;

/**
 * @author yangjunda1
 * @description
 * @date 5/8/19 4:46 PM
 */
public interface BaseTransport {

    void start();

    /**
     * 连接
     */
    void doConnect();

    /**
     * 关闭
     */
    void shutdown();

    /**
     * 是否开启
     *
     * @return the boolean
     */
    boolean isOpen();

    /**
     * 发送数据
     *
     * @param requestMessage
     * @return
     */
    ResponseMessage send0(RequestMessage requestMessage, int timeout);
}
