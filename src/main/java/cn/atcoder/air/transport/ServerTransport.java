package cn.atcoder.air.transport;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 7:20 PM
 */
public interface ServerTransport {

    Boolean start();

    void stop();
}
