package cn.atcoder.air.transport;

/**
 * @author yangjunda1
 * @description
 * @date 5/9/19 5:20 PM
 */
public class ClientTransportFactory {

    private static TcpClientTransport transport;

    public static TcpClientTransport refer() {
        if (transport == null) {

        }
        return transport;
    }

    public static void build(TcpClientTransport transport) {
        ClientTransportFactory.transport = transport;
    }
}
