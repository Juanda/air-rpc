package cn.atcoder.air;

import cn.atcoder.air.config.ProviderConfig;
import cn.atcoder.air.ins.HelloService;
import cn.atcoder.air.ins.HelloServiceImpl;
import cn.atcoder.air.transport.ServerTransport;
import cn.atcoder.air.transport.TcpServerTransport;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author yangjunda1
 * @description
 * @date 5/10/19 6:56 PM
 */
public class ServiceMainTest {

    @Test
    public void main() throws InterruptedException {
        ServerTransport serverTransport = new TcpServerTransport(19999);
        serverTransport.start();
        HelloService helloService = new HelloServiceImpl();
        ProviderConfig<HelloService> providerConfig = new ProviderConfig<>();
        providerConfig.setClazz(HelloService.class);
        providerConfig.setRef(helloService);
        providerConfig.setServer(serverTransport);
        providerConfig.export();

        while (true){
            TimeUnit.SECONDS.sleep(15);
        }
    }
}
