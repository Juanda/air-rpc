package cn.atcoder.air.ins;

/**
 * @author yangjunda1
 * @description
 * @date 5/10/19 2:03 PM
 */
public class HelloServiceImpl implements HelloService {
    @Override
    public String hello(String a, String b) {
        return a + "->" + b;
    }
}
