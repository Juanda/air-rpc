package cn.atcoder.air.ins;

/**
 * @author yangjunda1
 * @description
 * @date 5/10/19 2:02 PM
 */
public interface HelloService {
    String hello(String a, String b);

}
