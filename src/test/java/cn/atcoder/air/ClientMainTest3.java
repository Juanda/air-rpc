package cn.atcoder.air;

import cn.atcoder.air.config.ConsumerConfig;
import cn.atcoder.air.ins.HelloService;
import cn.atcoder.air.transport.TcpClientTransport;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author yangjunda1
 * @description
 * @date 5/10/19 7:04 PM
 */
public class ClientMainTest3 {
    static Logger log = LoggerFactory.getLogger(ClientMainTest3.class);
    static Executor executor = Executors.newSingleThreadExecutor();
    @Test
    public  void main() throws Exception {
        TcpClientTransport clientTransport = new TcpClientTransport("127.0.0.1", 19999);
        clientTransport.start();
        Thread.sleep(3000);

        ConsumerConfig<HelloService> consumerConfig = new ConsumerConfig<>();
        consumerConfig.setClazz(HelloService.class);
        consumerConfig.setInterfaceId(HelloService.class.getName());
        new Thread(() -> {
            try {
                sendData2(consumerConfig);
            } catch (InterruptedException e) {
            }
        }).start();
        while (true){
            TimeUnit.SECONDS.sleep(15);
        }
    }
    public static void sendData2(ConsumerConfig<HelloService> consumerConfig) throws InterruptedException {
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < 10000; i++) {
            String content = "client msg 3231312313123" + i + "Netty是由JBOSS提供的一个java开源框架。Netty提供异步的、事件驱动的网络应用程序框架和工具，用以快速开发高性能、高可靠性的网络服务器和客户端程序。\n" +
                    "也就是说，Netty 是一个基于NIO的客户、服务器端编程框架，使用Netty 可以确保你快速和简单的开发出一个网络应用，例如实现了某种协议的客户、服务端应用。Netty相当于简化和流线化了网络应用的编程开发过程，例如：基于TCP和UDP的socket服务开发。\n" +
                    "“快速”和“简单”并不用产生维护性或性能上的问题。Netty 是一个吸收了多种协议（包括FTP、SMTP、HTTP等各种二进制文本协议）的实现经验，并经过相当精心设计的项目。最终，Netty 成功的找到了一种方式，在保证易于开发的同时还保证了其应用的性能，稳定性和伸缩性。";
            content+=content;
            content+=content;
            content+=content;
            content+=content;
            content+=content;
            content+=content;
            content+=content;
            log.info("send"+i);
            String s = String.valueOf(i);
            String finalContent = content;
//            consumerConfig.refer().hello(finalContent, s);
            executor.execute(()->consumerConfig.refer().hello(finalContent, s));
//            Thread.sleep(2000L);
            Thread.sleep(random.nextInt(20000));
        }
    }
}
